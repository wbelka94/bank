package bank.operacje_bankowe;

import java.util.Date;

public class OperacjaPrzelewuMiedzybankowego extends OperacjaBankowa {
    private long idBanku;
    private long idRachunku;
    private long kwota;
    private long idBankuNadawcy;

    public OperacjaPrzelewuMiedzybankowego(long idBanku, long idRachunku, long kwota, long idBankuNadawcy) {
        this.idBanku = idBanku;
        this.idRachunku = idRachunku;
        this.kwota = kwota;
    }

    public long getIdBanku() {
        return idBanku;
    }

    public long getIdRachunku() {
        return idRachunku;
    }

    public long getKwota() {
        return kwota;
    }

    public long getIdBankuNadawcy() { return idBankuNadawcy; }

    @Override
    public OperacjaBankowa wykonaj() {
        //todo
        return null;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.PRZELEW_MIEDZYBANOWY;
        this.opis = "Przelew mi?dzybankowy";
        this.data_realizacji = new Date();
    }
}
