package bank.operacje_bankowe;

import bank.produkty_bankowe.RachunekBankowy;

import java.util.Date;

public class OperacjaPrzelewu extends OperacjaBankowa {
    private RachunekBankowy rachunekWyplata_;
    private RachunekBankowy rachunekWplata_;
    private long kwota_;

    public OperacjaPrzelewu(RachunekBankowy rachunekWyplata, RachunekBankowy rachunekWplata, long kwota) {
        rachunekWyplata_ = rachunekWyplata;
        rachunekWplata_ = rachunekWplata;
        kwota_ = kwota;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.PRZELEW;
        this.opis = "Przelew";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        if(rachunekWyplata_.wyplac(kwota_)){
            rachunekWplata_.wplac(kwota_);
        }
        rachunekWplata_.historia.dodaj(this);
        return this;
    }
}
