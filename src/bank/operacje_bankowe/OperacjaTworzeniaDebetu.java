package bank.operacje_bankowe;

import bank.produkty_bankowe.Debet;
import bank.produkty_bankowe.RachunekBankowy;

import java.util.Date;

public class OperacjaTworzeniaDebetu extends OperacjaBankowa {

    private RachunekBankowy rachunek_bankowy;
    private long limit;
    private Debet debet;

    public OperacjaTworzeniaDebetu(RachunekBankowy rachunek_bankowy, long limit) {
        this.rachunek_bankowy = rachunek_bankowy;
        this.limit = limit;
    }


    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.TWORZENIE_DEBETU;
        this.opis = "Utworzono debet na rachunku bankowym";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        debet = new Debet(rachunek_bankowy, limit);
        return this;
    }

    public Debet getDebet() {
        return debet;
    }

}
