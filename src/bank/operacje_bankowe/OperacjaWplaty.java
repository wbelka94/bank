package bank.operacje_bankowe;

import bank.produkty_bankowe.RachunekBankowy;

import java.util.Date;

public class OperacjaWplaty extends OperacjaBankowa {
    private RachunekBankowy rachunekBankowy_;
    private long kwota_;

    public OperacjaWplaty(RachunekBankowy rachunek_bankowy, long kwota) {
        rachunekBankowy_ = rachunek_bankowy;
        kwota_ = kwota;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.WPLATA;
        this.opis = "Wplata na konto bankowe";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        rachunekBankowy_.wplac(kwota_);
        return this;
    }
}
