package bank.operacje_bankowe;

import bank.produkty_bankowe.Kredyt;

import java.util.Date;

public class OperacjaSplatyKredytu extends OperacjaBankowa {
    private Kredyt kredyt;

    public OperacjaSplatyKredytu(Kredyt kredyt) {
        this.kredyt = kredyt;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.SPLATA_KREDYTU;
        this.opis = "Splata kredytu";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        kredyt.splac();
        return this;
    }
}
