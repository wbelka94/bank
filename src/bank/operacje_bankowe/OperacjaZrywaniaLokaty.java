package bank.operacje_bankowe;

import bank.produkty_bankowe.Lokata;

import java.util.Date;

public class OperacjaZrywaniaLokaty extends OperacjaBankowa {
    private final Lokata lokata;

    public OperacjaZrywaniaLokaty(Lokata lokata) {
        this.lokata = lokata;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.ZRYWANIE_LOKATY;
        this.opis = "Założono kredyt";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        lokata.zakoncz();
        return this;
    }
}
