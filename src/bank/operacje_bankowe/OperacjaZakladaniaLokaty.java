package bank.operacje_bankowe;

import bank.produkty_bankowe.Lokata;
import bank.odsetki.Odsetki;
import bank.produkty_bankowe.RachunekBankowy;

import java.util.Date;

public class OperacjaZakladaniaLokaty extends OperacjaBankowa{
    private Lokata lokata;
    private RachunekBankowy rachunek;
    private long kwota;
    private Odsetki mechanizmOdsetkowy;
    private Date dataZakonczenia;

    public OperacjaZakladaniaLokaty(RachunekBankowy rachunek, long kwota, Odsetki mechanizmOdsetkowy, Date dataZakonczenia) {
        this.rachunek = rachunek;
        this.kwota = kwota;
        this.mechanizmOdsetkowy = mechanizmOdsetkowy;
        this.dataZakonczenia = dataZakonczenia;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.ZAKLADANIE_KREDYTU;
        this.opis = "Założono kredyt";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        lokata = new Lokata(rachunek,new Date(),dataZakonczenia, mechanizmOdsetkowy, kwota);
        return this;
    }

    public Lokata getLokata() {
        return lokata;
    }
}
