package bank.operacje_bankowe;

import bank.odsetki.Odsetki;
import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.RachunekBankowy;

import java.util.Date;

public class OperacjaZaciaganiaKredytu extends OperacjaBankowa {
    private Kredyt kredyt;
    private RachunekBankowy rachunek;
    private long kwota;
    private Odsetki mechanizmOdsetkowy;

    public OperacjaZaciaganiaKredytu(RachunekBankowy rachunek, long kwota, Odsetki mechanizmOdsetkwoy) {
        this.rachunek = rachunek;
        this.kwota = kwota;
        this.mechanizmOdsetkowy = mechanizmOdsetkwoy;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.ZACIAGANIE_KREDYTU;
        this.opis = "Zaciągnięto kredyt";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        kredyt = new Kredyt(rachunek,kwota,mechanizmOdsetkowy);
        //rachunek.wykonajOperacje(this);
        rachunek.historia.dodaj(this);
        return this;
    }

    public Kredyt getKredyt() {
        return kredyt;
    }

}
