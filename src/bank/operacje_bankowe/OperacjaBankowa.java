package bank.operacje_bankowe;

import java.util.Date;

public abstract class OperacjaBankowa {
    TypOperacjiBankowej typ;
    Date data_realizacji;
    String opis;

    public OperacjaBankowa(){
        setMeta();
    }

    public abstract void setMeta();

    public abstract OperacjaBankowa wykonaj();
}
