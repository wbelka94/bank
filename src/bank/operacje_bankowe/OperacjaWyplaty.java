package bank.operacje_bankowe;

import bank.produkty_bankowe.RachunekBankowy;

import java.util.Date;

public class OperacjaWyplaty extends OperacjaBankowa {
    private RachunekBankowy rachunekBankowy_;
    private long kwota_;

    public OperacjaWyplaty(RachunekBankowy rachunek_bankowy, long kwota) {
        rachunekBankowy_ = rachunek_bankowy;
        kwota_ = kwota;
    }

    @Override
    public void setMeta() {
        this.typ = TypOperacjiBankowej.WYPLATA;
        this.opis = "Wyplata z konta bankowego";
        this.data_realizacji = new Date();
    }

    @Override
    public OperacjaBankowa wykonaj() {
        rachunekBankowy_.wyplac(kwota_);
        return this;
    }
}
