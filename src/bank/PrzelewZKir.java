package bank;

public class PrzelewZKir implements Comparable<PrzelewZKir> {
    private long bankId;
    private long idRachunku;
    private long kwota;
    private long bankIdNadawcy;

    public PrzelewZKir(long bankId, long idRachunku, long kwota, long bankIdNadawcy) {
        this.bankId = bankId;
        this.idRachunku = idRachunku;
        this.kwota = kwota;
    }

    public int compareTo(PrzelewZKir that) {
        return Long.compare(bankId, that.bankId);
    }

    public long getBankId() {
        return bankId;
    }

    public long getIdRachunku() {
        return idRachunku;
    }

    public long getKwota() {
        return kwota;
    }

    public long getBankIdNadawcy() {
        return bankIdNadawcy;
    }
}
