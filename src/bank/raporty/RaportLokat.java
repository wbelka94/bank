package bank.raporty;

import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RaportLokat implements Raport {

    private List<Lokata> listaLokat;

    public RaportLokat(List<ProduktBankowy> produktyBankowe){
        listaLokat = new ArrayList<Lokata>();
        Iterator<ProduktBankowy> iterator = produktyBankowe.iterator();
        while (iterator.hasNext()){
            iterator.next().acceptRaport(this);
        }
    }

    @Override
    public void visitRachunekBankowy(RachunekBankowy rachunekBankowy) {
    }

    @Override
    public void visitKredyt(Kredyt kredyt) {
    }

    @Override
    public void visitLokata(Lokata lokata) {
        listaLokat.add(lokata);
    }

    @Override
    public List<Lokata> getRaport(){
        return listaLokat;
    }
}
