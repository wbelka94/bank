package bank.raporty;

import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RaportRachunkowBankowych implements Raport {

    private List<RachunekBankowy> listaRachunkowBankowych;

    public RaportRachunkowBankowych(List<ProduktBankowy> produktyBankowe){
        listaRachunkowBankowych = new ArrayList<RachunekBankowy>();
        Iterator<ProduktBankowy> iterator = produktyBankowe.iterator();
        while (iterator.hasNext()){
            iterator.next().acceptRaport(this);
        }
    }

    @Override
    public void visitRachunekBankowy(RachunekBankowy rachunekBankowy) {
        listaRachunkowBankowych.add(rachunekBankowy);
    }

    @Override
    public void visitKredyt(Kredyt kredyt) {
    }

    @Override
    public void visitLokata(Lokata lokata) {
    }

    @Override
    public List<RachunekBankowy> getRaport(){
        return listaRachunkowBankowych;
    }
}
