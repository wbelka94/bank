package bank.raporty;

import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RaportKredytow implements Raport {

    private List<Kredyt> listaKredytow;

    public RaportKredytow(List<ProduktBankowy> produktyBankowe){
        listaKredytow = new ArrayList<Kredyt>();
        Iterator<ProduktBankowy> iterator = produktyBankowe.iterator();
        while (iterator.hasNext()){
            iterator.next().acceptRaport(this);
        }

    }

    @Override
    public void visitRachunekBankowy(RachunekBankowy rachunekBankowy) {

    }

    @Override
    public void visitKredyt(Kredyt kredyt) {
        listaKredytow.add(kredyt);
    }

    @Override
    public void visitLokata(Lokata lokata) {
    }

    @Override
    public List<Kredyt> getRaport(){
        return listaKredytow;
    }

}
