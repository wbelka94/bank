package bank.raporty;

import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.RachunekBankowy;

public interface Raport {
    void visitRachunekBankowy(RachunekBankowy rachunekBankowy);
    void visitKredyt(Kredyt rachunekBankowy);
    void visitLokata(Lokata lokata);
    Object getRaport();
}
