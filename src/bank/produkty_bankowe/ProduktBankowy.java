package bank.produkty_bankowe;

import bank.operacje_bankowe.OperacjaBankowa;
import bank.raporty.Raport;

public interface ProduktBankowy {
    boolean wplac(long kwota);
    boolean wyplac(long kwota);

    OperacjaBankowa wykonajOperacje(OperacjaBankowa operacjaBankowa);

    void naliczOdsetki();

    long getSaldo();

    void acceptRaport(Raport raport);
}
