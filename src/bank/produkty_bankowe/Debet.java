package bank.produkty_bankowe;

import bank.operacje_bankowe.OperacjaBankowa;
import bank.raporty.Raport;

public class Debet implements ProduktBankowy{
    private int ujemneSaldo;
    private long max;

    private ProduktBankowy produktBankowy;

    public int getUjemneSaldo(){
        return ujemneSaldo;
    }
    public long getMax() { return max; }

    public Debet(ProduktBankowy produktBankowy, long max) {
        this.produktBankowy = produktBankowy;
        this.max = max;
        ujemneSaldo = 0;
    }

    /**
     * Pomniejsza wielkość debetu o wartość parametru kwota
     * @param kwota
     * @return true w przypadku poowodzenia lub false w przeciwnym wypadku
     */
    @Override
    public boolean wyplac(long kwota){
        if(produktBankowy.getSaldo() < kwota){
            kwota -= produktBankowy.getSaldo();
            if(max >= ujemneSaldo + kwota) {
                produktBankowy.wyplac(produktBankowy.getSaldo());
                ujemneSaldo += kwota;
                return true;
            }
        }
        return produktBankowy.wyplac(kwota);
    }


    /**
     * Zmniejsza wielkość debetu o zadaną kwote
     * @param kwota - wielkość wplaty
     * @return true w przypadku poowodzenia lub false w przeciwnym wypadku
     */
    @Override
    public boolean wplac(long kwota) {
        if(ujemneSaldo > 0){
            if(kwota > ujemneSaldo){
                kwota -= ujemneSaldo;
                if(produktBankowy.wplac(kwota)){
                    ujemneSaldo = 0;
                    return true;
                }
            }
            else{
                ujemneSaldo -= kwota;
                return true;
            }
        }
        return produktBankowy.wplac(kwota);
    }

    @Override
    public OperacjaBankowa wykonajOperacje(OperacjaBankowa operacjaBankowa) {
        return produktBankowy.wykonajOperacje(operacjaBankowa);
    }

    @Override
    public void naliczOdsetki() {
        produktBankowy.naliczOdsetki();
    }

    @Override
    public long getSaldo() {
        return produktBankowy.getSaldo() - ujemneSaldo;
    }

    @Override
    public void acceptRaport(Raport raport) {
        produktBankowy.acceptRaport(raport);
    }
}
