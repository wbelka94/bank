package bank.produkty_bankowe;

import bank.Historia;
import bank.odsetki.Odsetki;
import bank.operacje_bankowe.OperacjaBankowa;
import bank.raporty.Raport;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class RachunekBankowy implements ProduktBankowy {
    private long id;
    private int id_wlasciciela;
    public Historia historia;
    private Odsetki mechanizmOdsetkowy;
    private long saldo;
    private Date data_zalozenia;
   // private List<Lokata> listaLokat;
    private static final AtomicLong counter = new AtomicLong(1);
    // TODO dodanie mechanizmu odsetkowego

//    public void setDebet(Debet debet) {
//        this.debet = debet;
//    }
//    public Debet getDebet() { return debet; }
    public long getId() { return id; }

//    public void dodajLokate(Lokata lokata) { listaLokat.add(lokata); }

  //  private Debet debet;

    public RachunekBankowy(int id_wlasciciela, Odsetki mechanizmOdsetkowy){
        this.mechanizmOdsetkowy = mechanizmOdsetkowy;
        this.historia = new Historia();
        this.saldo = 0;
        this.id_wlasciciela = id_wlasciciela;
        this.id = counter.getAndIncrement();
    }

    public boolean wplac(long kwota){
        if(kwota < 0) {
            return false;
        }
        saldo += kwota;
        System.out.println("Saldo: " + this.saldo);
        return true;
    }

    public boolean wyplac(long kwota){
        if(kwota < 0){
            return false;
        }
        if(this.saldo >= kwota){
            this.saldo -= kwota;
            System.out.println("Saldo: "+this.saldo);
            return true;
        }
        System.out.println("Operacja się nie udała - za mało środków");
        return false;
    }

    @Override
    public OperacjaBankowa wykonajOperacje(OperacjaBankowa operacjaBankowa) {
        operacjaBankowa.wykonaj();
        historia.dodaj(operacjaBankowa);
        return operacjaBankowa;
    }

    @Override
    public void naliczOdsetki() {

    }

    @Override
    public long getSaldo() {
        return saldo;
    }

    @Override
    public void acceptRaport(Raport raport) {
        raport.visitRachunekBankowy(this);
    }
}
