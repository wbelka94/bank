package bank.produkty_bankowe;

import bank.Historia;
import bank.odsetki.Odsetki;
import bank.operacje_bankowe.OperacjaBankowa;
import bank.raporty.Raport;

public final class Kredyt implements ProduktBankowy {
    private  ProduktBankowy produktBankowy;
    private Odsetki mechanizmOdsetkwoy;
    private long kwotaKredytu;
    private Historia historia;

    public Kredyt(ProduktBankowy produktBankowy, long kwotaKredytu, Odsetki mechanizmOdsetkwoy) {
        this.produktBankowy = produktBankowy;
        this.kwotaKredytu = kwotaKredytu;
        this.mechanizmOdsetkwoy = mechanizmOdsetkwoy;
        this.produktBankowy = produktBankowy;
        historia = new Historia();
    }


    public boolean splac() {
        long kwotaDoSplaty = kwotaKredytu + mechanizmOdsetkwoy.przelicz(this);
        if(produktBankowy.wyplac(kwotaDoSplaty)){
            kwotaKredytu = 0;
            return true;
        }
        return false;
    }

    @Override
    public boolean wplac(long kwota) {
        return false;
    }

    @Override
    public boolean wyplac(long kwota) {
        return false;
    }

    @Override
    public OperacjaBankowa wykonajOperacje(OperacjaBankowa operacjaBankowa) {
        operacjaBankowa.wykonaj();
        historia.dodaj(operacjaBankowa);
        return operacjaBankowa;
    }

    @Override
    public void naliczOdsetki() {

    }

    @Override
    public long getSaldo() {
        return this.kwotaKredytu;
    }

    @Override
    public void acceptRaport(Raport raport) {
        raport.visitKredyt(this);
    }
}
