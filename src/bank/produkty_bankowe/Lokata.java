package bank.produkty_bankowe;

import bank.Historia;
import bank.odsetki.Odsetki;
import bank.operacje_bankowe.OperacjaBankowa;
import bank.raporty.Raport;
import javafx.scene.input.DataFormat;

import java.util.Date;

public final class Lokata implements ProduktBankowy{
    private long saldo;
    private Date dataZalozenia;
    private Date dataZakonczenia;
    private ProduktBankowy produktBankowy;
    Odsetki mechanizmOdsetkowy;
    private Historia historia;

    public Lokata(ProduktBankowy produktBankowy, Date dataZalozenia, Date dataZakonczenia, Odsetki mechanizmOdsetkowy, long kwota) {
        this.produktBankowy = produktBankowy;
        this.dataZalozenia = dataZalozenia;
        this.dataZakonczenia = dataZakonczenia;
        this.mechanizmOdsetkowy = mechanizmOdsetkowy;
        this.saldo = kwota;
        historia = new Historia();
    }

    public boolean zakoncz(){
        if(dataZakonczenia.before(new Date())){
            saldo += mechanizmOdsetkowy.przelicz(this);
        }
        if(produktBankowy.wplac(saldo)) {
            saldo = 0;
        }
        return true;
    }

    public Date getDataZalozenia() { return dataZalozenia; }

    public Date getDataZakonczenia() { return dataZakonczenia; }


    @Override
    public boolean wplac(long kwota) {
        return false;
    }

    @Override
    public boolean wyplac(long kwota) {
        return false;
    }

    @Override
    public OperacjaBankowa wykonajOperacje(OperacjaBankowa operacjaBankowa) {
        operacjaBankowa.wykonaj();
        historia.dodaj(operacjaBankowa);
        return operacjaBankowa;
    }

    @Override
    public void naliczOdsetki() {

    }

    @Override
    public long getSaldo() {
        return saldo;
    }

    @Override
    public void acceptRaport(Raport raport) {
        raport.visitLokata(this);
    }
}
