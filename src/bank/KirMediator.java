package bank;

import bank.operacje_bankowe.OperacjaPrzelewuMiedzybankowego;

import java.util.*;

public class KirMediator {
    private Map<Long, Bank> listaBankow = new HashMap<>();
    private static KirMediator instance = null;

    public static KirMediator getInstance() {
        if(instance != null) {
            return instance;
        } else {
            instance = new KirMediator();
            return instance;
        }
    }

    public void dodajBank(Bank bank) {
        listaBankow.put(bank.getBankId(), bank);
    }

    private KirMediator() { }

    public void przyjmijListePrzelewow(List<OperacjaPrzelewuMiedzybankowego> listaPrzelewow) {
        //przemiana listy OperacjaPrzelewuMiedzybankowego na liste PrzelewZKir
        List<PrzelewZKir> listaPrzelewowZKir = new ArrayList<>();
        for (OperacjaPrzelewuMiedzybankowego przelew : listaPrzelewow) {
            long bankId = przelew.getIdBanku();
            long idRachunku = przelew.getIdRachunku();
            long kwota = przelew.getKwota();
            long bankIdNadawcy = przelew.getIdBankuNadawcy();
            listaPrzelewowZKir.add(new PrzelewZKir(bankId, idRachunku, kwota, bankIdNadawcy));
        }
        //posortowanie na paczki do poszczególnych banków
        Collections.sort(listaPrzelewowZKir);
        long poprzednieBankId = 0;
        int nrPaczki = -1;
        List<ArrayList<PrzelewZKir>> listaPaczekPrzelewow = new ArrayList<>();
        for (PrzelewZKir przelew : listaPrzelewowZKir) {
            if(poprzednieBankId != przelew.getBankId()) {
                listaPaczekPrzelewow.add(new ArrayList<>());
                poprzednieBankId = przelew.getBankId();
                nrPaczki++;
                listaPaczekPrzelewow.get(nrPaczki).add(przelew);
            } else {
                listaPaczekPrzelewow.get(nrPaczki).add(przelew);
            }
        }

        for (ArrayList<PrzelewZKir> lista : listaPaczekPrzelewow) {
            wyslijPrzelewy(lista);
        }
    }

    private void wyslijPrzelewy(List<PrzelewZKir> listaPrzelewow) {
        if(listaPrzelewow == null || listaPrzelewow.size() == 0) {
            return;
        }

        long bankId = listaPrzelewow.get(0).getBankId();
        Bank bank = listaBankow.get(bankId);
        bank.przymijPrzelewyZKir(listaPrzelewow);
    }
}
