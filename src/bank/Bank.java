package bank;

import bank.odsetki.Odsetki;
import bank.odsetki.OdsetkiA;
import bank.operacje_bankowe.*;
//import bank.produkty_bankowe.Kredyt;
//import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;
import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Bank {
    private List<ProduktBankowy> produkty;
    private Historia historia;
    private Long bankId;
    private static final AtomicLong counter = new AtomicLong(1);
    private List<OperacjaPrzelewuMiedzybankowego> listaPrzelewowMiedzybankowych;

    public Bank() {
        this.produkty = new ArrayList<>();
        historia = new Historia();
        this.bankId = counter.getAndIncrement();
    }
    public Bank(List<RachunekBankowy> rachunki) {
        this.produkty = new ArrayList<>();
        this.produkty.addAll(rachunki);
        this.bankId = counter.getAndIncrement();
    }

    /**
     * Pozwala na założenie nowego rachunku w banku
     * @return
     */
    public RachunekBankowy zalozRachunek(int id_wlasciciela) {
        RachunekBankowy rachunek_bankowy = new RachunekBankowy(id_wlasciciela, new OdsetkiA(10));
        produkty.add(rachunek_bankowy);
        return rachunek_bankowy;
    }

    /**
     * Umożliwia wpłatę środków na wybrany rachunek
     * @param rachunek
     * @param kwota
     * @return
     */
    public OperacjaBankowa wplac(RachunekBankowy rachunek, long kwota){
        OperacjaWplaty operacjaWplaty = new OperacjaWplaty(rachunek,kwota);
        rachunek.wykonajOperacje(operacjaWplaty);
        historia.dodaj(operacjaWplaty);
        return operacjaWplaty;
    }

    /**
     * Umożliwja wypłatę środków z wybranego rachunku
     * @param rachunek
     * @param kwota
     * @return
     */
    public OperacjaBankowa wyplac(RachunekBankowy rachunek, long kwota) {
        OperacjaWyplaty operacjaBankowa = new OperacjaWyplaty(rachunek, kwota);
        rachunek.wykonajOperacje(operacjaBankowa);
        historia.dodaj(operacjaBankowa);
        return operacjaBankowa;
    }


    public OperacjaBankowa przelew(RachunekBankowy rachunek_z, RachunekBankowy rachunek_do, long kwota){
        OperacjaBankowa operacjaBankowa = new OperacjaPrzelewu(rachunek_z,rachunek_do,kwota);
        rachunek_z.wykonajOperacje(operacjaBankowa);
        return operacjaBankowa;
    }

    // TODO nalicz_odsetki,
    // TODO zmien_mechanizm_odsetkowy,

   /* public OperacjaBankowa zaloz_lokate(RachunekBankowy rachunek, long kwota, Odsetki mechanizm_odsetkowy, Date dataZakonczenia){
        OperacjaZakladaniaLokaty operacja = new OperacjaZakladaniaLokaty(rachunek,kwota,mechanizm_odsetkowy,dataZakonczenia);
        produkty.add(operacja.getLokata());
        this.historia.dodaj(operacja);
        return operacja;
    }*/

//    public OperacjaBankowa zerwij_lokate(Lokata lokata){
//        return new OperacjaZrywaniaLokaty(lokata);
//    }

 /*   public OperacjaBankowa zaciagnij_kredyt(RachunekBankowy rachunek, long kwota, float procent, int liczba_rat){
        OperacjaZaciaganiaKredytu operacja = new OperacjaZaciaganiaKredytu(rachunek,kwota,procent,liczba_rat);
        produkty.add(operacja.getKredyt());
        return operacja;
    }*/
//
//    public OperacjaBankowa splac_kredyt(Kredyt kredyt, long kwota){
//        return new OperacjaSplatyKredytu(kredyt, kwota);
//    }

//    /**
//     * Umożliwia stworzenie debetu na wybranym rachunku
//     * @param rachunek
//     * @param limit
//     * @return
//     */
//    public OperacjaBankowa stworzDebet(RachunekBankowy rachunek, long limit){
//        OperacjaTworzeniaDebetu operacjaTworzeniaDebetu = new OperacjaTworzeniaDebetu(rachunek, limit);
//        rachunek.wykonajOperacje(operacjaTworzeniaDebetu);
//        historia.dodaj(operacjaTworzeniaDebetu);
//        return operacjaTworzeniaDebetu;
//    }

    // TODO wykonaj_raport

    public void wykonajOperacje(ProduktBankowy produktBankowy, OperacjaBankowa operacjaBankowa) {
        historia.dodaj(produktBankowy.wykonajOperacje(operacjaBankowa));
    }

    public Long getBankId() {
        return bankId;
    }

    public void DodajPrzelewDoListyPrzelewowMiedzybankowych(OperacjaPrzelewuMiedzybankowego operacja){
        listaPrzelewowMiedzybankowych.add(operacja);
    }

    public void przelewMiedzyBankowy(){
        KirMediator mediator = KirMediator.getInstance();
        mediator.przyjmijListePrzelewow(listaPrzelewowMiedzybankowych);
        listaPrzelewowMiedzybankowych.clear();
    }

    public void przymijPrzelewyZKir(List<PrzelewZKir> listaPrzelewowZKir) {
        for (PrzelewZKir przelewZKir : listaPrzelewowZKir) {
            long bankIdNadawcy = przelewZKir.getBankIdNadawcy();
            long idRachunku = przelewZKir.getIdRachunku();
            long kwota = przelewZKir.getKwota();
            RachunekBankowy rachunekBankowy = znajdzRachunakOId(idRachunku);
            if(rachunekBankowy == null) {
                //todo
            } else {
                //todo
            }
        }
    }

    public RachunekBankowy znajdzRachunakOId(long id) {
        for (ProduktBankowy produkt : produkty) {
            if(produkt instanceof RachunekBankowy) {
                if(((RachunekBankowy) produkt).getId() == id) {
                    return (RachunekBankowy) produkt;
                }
            }
        }
        return null;
    }
}
