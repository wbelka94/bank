package bank;

import bank.produkty_bankowe.RachunekBankowy;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("Uruchomiono bank");
        Bank bank = new Bank();
        Bank bank2 = new Bank();
        List<Bank> listaBankow = new ArrayList<>();
        listaBankow.add(bank);
        listaBankow.add(bank2);
        KirMediator kir = KirMediator.getInstance();
        kir.dodajBank(bank);
        kir.dodajBank(bank2);
//        bank.dodajRachunek(new Rachunek(new Osoba("Adam", "Kowalski",1), 1000.0,0.0, false));
//        bank.dodajRachunek(new Rachunek(new Osoba("Jan", "Nowak",2), 1200.3,0.0, true));
        RachunekBankowy r = bank.zalozRachunek(1);
        System.out.println(bank.wplac(r,10));
        System.out.println(bank.wplac(r,10));
        System.out.println(bank.wyplac(r,10));
      //  System.out.println(bank.stworzDebet(r,1000));
        System.out.println(bank.wyplac(r,100));

    }
}
