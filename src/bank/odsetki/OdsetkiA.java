package bank.odsetki;

import bank.produkty_bankowe.ProduktBankowy;

public class OdsetkiA implements Odsetki {

    private float procent;

    public OdsetkiA(float procent){
        this.procent = procent;
    }

    @Override
    public long przelicz(ProduktBankowy produktBankowy) {
        return (long) (produktBankowy.getSaldo() * procent);
    }
}
