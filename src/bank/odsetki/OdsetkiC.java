package bank.odsetki;

import bank.produkty_bankowe.ProduktBankowy;

public class OdsetkiC implements Odsetki{
    @Override
    public long przelicz(ProduktBankowy produktBankowy) {
        if(produktBankowy.getSaldo()>10000000) {
            return (long) (produktBankowy.getSaldo()*0.1);
        } else {
            return (long) (produktBankowy.getSaldo()*0.05);
        }
    }
}
