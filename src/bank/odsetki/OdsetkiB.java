package bank.odsetki;

import bank.produkty_bankowe.ProduktBankowy;

public class OdsetkiB implements Odsetki{
    @Override
    public long przelicz(ProduktBankowy produktBankowy) {
        if(produktBankowy.getSaldo()>500000) {
            return (long) (produktBankowy.getSaldo()*0.05);
        } else {
            return (long) (produktBankowy.getSaldo()*0.03);
        }
    }
}
