package bank.odsetki;

import bank.produkty_bankowe.ProduktBankowy;

public interface Odsetki {
    long przelicz(ProduktBankowy produktBankowy);
}
