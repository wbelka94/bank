package bank;

import bank.operacje_bankowe.OperacjaBankowa;

import java.util.ArrayList;
import java.util.List;

public class Historia {
    private List<OperacjaBankowa> operacje_;

    public Historia() { this.operacje_ = new ArrayList<OperacjaBankowa>();}
    public Historia(List<OperacjaBankowa> operacje) {
        this.operacje_ = new ArrayList<OperacjaBankowa>();
        this.operacje_.addAll(operacje);
    }
    public boolean dodaj(OperacjaBankowa operacja) {
        this.operacje_.add(operacja);
        return true;
    }

    public OperacjaBankowa pobierzOperacjeONumerze(int nr) {
        return operacje_.get(nr);
    }
}
