package test.Odsetki;

import bank.odsetki.OdsetkiB;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OdsetkiBTest {
    OdsetkiB odsetki;

    @BeforeEach
    void setUp() {
        odsetki = new OdsetkiB();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void przeliczPonizej500000() {
        RachunekBankowy rachunekBankowy = new RachunekBankowy(1,odsetki);
        rachunekBankowy.wplac(100);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(3,kwota);
    }

    @Test
    void przelicz500000() {
        RachunekBankowy rachunekBankowy = new RachunekBankowy(1,odsetki);
        rachunekBankowy.wplac(500000);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(15000,kwota);
    }

    @Test
    void przelicz500001() {
        RachunekBankowy rachunekBankowy = new RachunekBankowy(1,odsetki);
        rachunekBankowy.wplac(500001);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(25000,kwota);
    }

    @Test
    void przeliczPowyzej500000() {
        RachunekBankowy rachunekBankowy = new RachunekBankowy(1,odsetki);
        rachunekBankowy.wplac(1000000);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(50000,kwota);
    }

}