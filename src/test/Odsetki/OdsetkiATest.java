package test.Odsetki;

import bank.odsetki.OdsetkiA;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OdsetkiATest {
    OdsetkiA odsetki;
    RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        odsetki = new OdsetkiA((float) 0.1);
        rachunekBankowy = new RachunekBankowy(1,odsetki);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void przelicz100() {
        rachunekBankowy.wplac(100);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(10,kwota);
    }

    @Test
    void przelicz0() {
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(0,kwota);
    }
}