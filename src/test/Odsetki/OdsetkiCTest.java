package test.Odsetki;

import bank.odsetki.OdsetkiA;
import bank.odsetki.OdsetkiB;
import bank.odsetki.OdsetkiC;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OdsetkiCTest {
    OdsetkiC odsetki;

    @BeforeEach
    void setUp() {
        odsetki = new OdsetkiC();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void przeliczPowyzej() {
        RachunekBankowy rachunekBankowy = new RachunekBankowy(1, odsetki);
        rachunekBankowy.wplac(10000010);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(1000001,kwota);
    }

    @Test
    void przeliczPonizej() {
        RachunekBankowy rachunekBankowy = new RachunekBankowy(1,odsetki);
        rachunekBankowy.wplac(10000000);
        long kwota = odsetki.przelicz(rachunekBankowy);
        assertEquals(500000,kwota);
    }
}