package test.Raporty;

import bank.odsetki.Odsetki;
import bank.odsetki.OdsetkiB;
import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;
import bank.raporty.Raport;
import bank.raporty.RaportKredytow;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.ref.PhantomReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RaportKredytowTest {

    private List<ProduktBankowy> produktyBankowe;
    private RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        produktyBankowe = new ArrayList<ProduktBankowy>();
        rachunekBankowy = new RachunekBankowy(1, new OdsetkiB());
        produktyBankowe.add(rachunekBankowy);
        produktyBankowe.add(new RachunekBankowy(2, new OdsetkiB()));
        produktyBankowe.add(new RachunekBankowy(3, new OdsetkiB()));
        produktyBankowe.add(new RachunekBankowy(4, new OdsetkiB()));
        produktyBankowe.add(new RachunekBankowy(5, new OdsetkiB()));
        produktyBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),1000));
        produktyBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),200));
        produktyBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),10030));
        produktyBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),1015));
        produktyBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),10666));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void raport10Kredytow(){
        for (int i = 0; i < 10; i++) {
            produktyBankowe.add(new Kredyt(rachunekBankowy, 1000, new OdsetkiB()));
        }

        Raport raport = new RaportKredytow(produktyBankowe);
        ArrayList<Kredyt> listaZRaportu = (ArrayList<Kredyt>) raport.getRaport();

        assertEquals(10,listaZRaportu.size());
    }
}