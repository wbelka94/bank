package test.Raporty;

import bank.odsetki.OdsetkiB;
import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;
import bank.raporty.Raport;
import bank.raporty.RaportKredytow;
import bank.raporty.RaportRachunkowBankowych;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import static org.junit.jupiter.api.Assertions.*;

class RaportRachunkowBankowychTest {
    private List<ProduktBankowy> rachunkiBankowe;
    private RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        rachunkiBankowe = new ArrayList<ProduktBankowy>();
        rachunkiBankowe.add(new RachunekBankowy(1, new OdsetkiB()));
        rachunkiBankowe.add(new RachunekBankowy(2, new OdsetkiB()));
        rachunkiBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),1000));
        rachunkiBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),200));
        rachunkiBankowe.add(new RachunekBankowy(3, new OdsetkiB()));
        rachunkiBankowe.add(new RachunekBankowy(4, new OdsetkiB()));
        rachunkiBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),10030));
        rachunkiBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),1015));
        rachunkiBankowe.add(new RachunekBankowy(5, new OdsetkiB()));
        rachunkiBankowe.add(new Lokata(rachunekBankowy,new Date(),new Date(),new OdsetkiB(),10666));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void raportRachunkowBankowych(){
        for (int i = 0; i < 10; i++) {
            rachunkiBankowe.add(new RachunekBankowy(i, new OdsetkiB()));
        }
        Raport raport = new RaportRachunkowBankowych(rachunkiBankowe);
        ArrayList<Kredyt> listaZRaportu = (ArrayList<Kredyt>) raport.getRaport();

        assertEquals(15,listaZRaportu.size());

    }
}