package test.ProduktyBankowe;


import bank.odsetki.OdsetkiA;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import bank.produkty_bankowe.RachunekBankowy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RachunekBankowyTest {

    private RachunekBankowy rachunek;

    @BeforeEach
    void setUp() {
        rachunek = new RachunekBankowy(1, new OdsetkiA(1));
    }

    @AfterEach
    void tearDown() {
        rachunek = null;
    }

    @Test
    void setDebet() {
    }

    @Test
    void zwieksz_saldo() {
        boolean result = rachunek.wplac(1000L);
        assertEquals(result,true);
    }

    @Test
    void zmniejsz_saldo_za_duza_kwota() {
        boolean result = rachunek.wyplac(1000L);
        assertEquals(result,false);
    }

    @Test
    void zmniejsz_saldo_prawidlowa_kwota() {
        rachunek.wplac(10000L);
        boolean result = rachunek.wyplac(1000L);
        assertEquals(result,true);
    }

    @Test
    void zmniejsz_saldo_kwota_ujemna() {
        rachunek.wplac(10000L);
        boolean result = rachunek.wyplac(-1000L);
        assertEquals(result,false);
    }
}