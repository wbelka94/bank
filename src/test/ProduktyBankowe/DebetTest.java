package test.ProduktyBankowe;

import bank.Bank;
import bank.odsetki.OdsetkiB;
import bank.produkty_bankowe.Debet;
import bank.produkty_bankowe.ProduktBankowy;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DebetTest {
    private RachunekBankowy rachunek;
    private ProduktBankowy produktBankowyZDebetem;

    @BeforeEach
    void setUp() {
        rachunek = new RachunekBankowy(1,new OdsetkiB());
        produktBankowyZDebetem = new Debet(rachunek,1000);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void wyplac100_saldo0_debet1000(){
        produktBankowyZDebetem.wyplac(100);
        assertEquals(-100, produktBankowyZDebetem.getSaldo());
    }

    @Test
    void wyplac1000_saldo0_debet1000(){
        produktBankowyZDebetem.wyplac(1000);
        assertEquals(-1000, produktBankowyZDebetem.getSaldo());
    }

    @Test
    void wyplac100_saldo1000_debet1000(){
        produktBankowyZDebetem.wplac(1000);
        produktBankowyZDebetem.wyplac(100);
        assertEquals(900, produktBankowyZDebetem.getSaldo());
    }

    @Test
    void wyplac2000_saldo0_debet1000(){
        produktBankowyZDebetem.wyplac(2000);
        assertEquals(0, produktBankowyZDebetem.getSaldo());
    }

}