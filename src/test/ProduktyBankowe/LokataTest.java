package test.ProduktyBankowe;

import bank.Bank;
import bank.odsetki.OdsetkiA;
import bank.odsetki.OdsetkiB;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class LokataTest {


    private Lokata lokata;
    private RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        rachunekBankowy = new RachunekBankowy(1,new OdsetkiB());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void zakonczLokatePrzedCzasem() throws ParseException {
        lokata = new Lokata(rachunekBankowy,new Date(), stringToDate("2018.09.25"), new OdsetkiA(10), 1000);
        lokata.zakoncz();
        assertEquals(1000,rachunekBankowy.getSaldo());

    }

    @Test
    void zakonczLokatePoCzasie() {
        lokata = new Lokata(rachunekBankowy,stringToDate("2018.01.25"), stringToDate("2018.02.25"), new OdsetkiA((float) 0.1), 1000);
        lokata.zakoncz();
        assertEquals(1100,rachunekBankowy.getSaldo());
    }

    private Date stringToDate(String string){
        DateFormat format = new SimpleDateFormat("yyyy.MM.dd");
        try {
            return format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}