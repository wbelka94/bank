package test.ProduktyBankowe;

import bank.odsetki.OdsetkiA;
import bank.odsetki.OdsetkiB;
import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.Lokata;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class KredytTest {


    private Kredyt kredyt;
    private RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        rachunekBankowy = new RachunekBankowy(1,new OdsetkiB());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void splataBrakSrodkow() throws ParseException {
        kredyt = new Kredyt(rachunekBankowy,1000, new OdsetkiA((float) 0.1));
        boolean czySplacono = kredyt.splac();
        assertEquals(false,czySplacono);
        assertEquals(0,rachunekBankowy.getSaldo());

    }

    @Test
    void splata() throws ParseException {
        rachunekBankowy.wplac(2000);
        kredyt = new Kredyt(rachunekBankowy,1000, new OdsetkiA((float) 0.1));
        boolean czySplacono = kredyt.splac();
        assertEquals(true,czySplacono);
        assertEquals(900,rachunekBankowy.getSaldo());

    }

}