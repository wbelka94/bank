//package test;
//
//import bank.Bank;
//import bank.produkty_bankowe.RachunekBankowy;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class BankTest {
//    private Bank bank;
//    private RachunekBankowy rachunek;
//    private RachunekBankowy rachunek2;
//    @BeforeEach
//    void setUp() {
//        bank = new Bank();
//        //rachunek = new RachunekBankowy(1);
//       // rachunek2 = new RachunekBankowy(2);
//    }
//
//    @AfterEach
//    void tearDown() {
//    }
//
//    @Test
//    void zalozRachunek() {
//        bank.zalozRachunek(1);
//    }
//
//    @Test
//    void wplac100() {
//        bank.wplac(rachunek, 10000);
//        assertEquals(10000, rachunek.getSaldo());
//    }
//    @Test
//    void wplac100koma12() {
//        bank.wplac(rachunek, 10012);
//        assertEquals(10012, rachunek.getSaldo());
//    }
//    @Test
//    void wplac200() {
//        bank.wplac(rachunek, 20000);
//        assertEquals(20000, rachunek.getSaldo());
//    }
//
//    @Test
//    void wplacMinus100() {
//        bank.wplac(rachunek, -10000);
//        assertEquals(0, rachunek.getSaldo());
//    }
//    @Test
//    void wplacMAX_VALUE() {
//        bank.wplac(rachunek, Long.MAX_VALUE);
//        assertEquals(Long.MAX_VALUE, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplac100() {
//        bank.wyplac(rachunek, 10000);
//        assertEquals(0, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplacminus100() {
//        bank.wyplac(rachunek, -10000);
//        assertEquals(0, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplac100gdySaldo100() {
//        bank.wplac(rachunek, 10000);
//        bank.wyplac(rachunek, 10000);
//        assertEquals(0, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplac100gdySaldo200() {
//        bank.wplac(rachunek, 20000);
//        bank.wyplac(rachunek, 10000);
//        assertEquals(10000, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplacMinus100gdySaldo200() {
//        bank.wplac(rachunek, 20000);
//        bank.wyplac(rachunek, -10000);
//        assertEquals(20000, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplac200gdySaldo100() {
//        bank.wplac(rachunek, 10000);
//        bank.wyplac(rachunek, 20000);
//        assertEquals(10000, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplacMinus200gdySaldo100() {
//        bank.wplac(rachunek, 10000);
//        bank.wyplac(rachunek, -20000);
//        assertEquals(10000, rachunek.getSaldo());
//    }
//    @Test
//    void wyplac99koma99gdySaldo100() {
//        bank.wplac(rachunek, 10000);
//        bank.wyplac(rachunek, 9999);
//        assertEquals(1, rachunek.getSaldo());
//    }
//
//    @Test
//    void wyplacMAX_VALUE() {
//        bank.wyplac(rachunek, Long.MAX_VALUE);
//        assertEquals(0, rachunek.getSaldo());
//    }
//    @Test
//    void wyplacMAX_VALUEgdySaldoMAX_VALUE() {
//        bank.wplac(rachunek, Long.MAX_VALUE);
//        bank.wyplac(rachunek, Long.MAX_VALUE);
//        assertEquals(0, rachunek.getSaldo());
//    }
//
////    @Test
////    void stworzDebet() {
////        bank.stworzDebet(rachunek, 20000);
////        assertEquals(20000, rachunek.getDebet().getMax());
////    }
////
////    @Test
////    void wyplac100gdyDebet100() {
////        bank.stworzDebet(rachunek, 10000);
////        bank.wyplac(rachunek, 10000);
////        assertEquals(0, rachunek.getSaldo());
////        assertEquals(10000, rachunek.getDebet().getUjemneSaldo());
////    }
////    @Test
////    void wyplac100gdyDebet50() {
////        bank.stworzDebet(rachunek, 5000);
////        bank.wyplac(rachunek, 10000);
////        assertEquals(0, rachunek.getSaldo());
////        assertEquals(0, rachunek.getDebet().getUjemneSaldo());
////    }
////    @Test
////    void wyplac100gdyDebet50Saldo50() {
////        bank.stworzDebet(rachunek, 5000);
////        bank.wplac(rachunek, 5000);
////        bank.wyplac(rachunek, 10000);
////        assertEquals(0, rachunek.getSaldo());
////        assertEquals(5000, rachunek.getDebet().getUjemneSaldo());
////    }
//
//    @Test
//    void przelew200_100(){
//        bank.wplac(rachunek,20000);
//        bank.przelew(rachunek,rachunek2,10000);
//        assertEquals(10000, rachunek.getSaldo());
//        assertEquals(10000,rachunek2.getSaldo());
//    }
//
//    @Test
//    void przelew200_300(){
//        bank.wplac(rachunek,20000);
//        bank.przelew(rachunek,rachunek2,30000);
//        assertEquals(20000, rachunek.getSaldo());
//        assertEquals(0,rachunek2.getSaldo());
//    }
//
//
//}