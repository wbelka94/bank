package test.OperacjeBankowe;

import bank.odsetki.OdsetkiA;
import bank.odsetki.OdsetkiB;
import bank.operacje_bankowe.OperacjaSplatyKredytu;
import bank.operacje_bankowe.OperacjaZaciaganiaKredytu;
import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperacjaSplatyKredytuTest {
    RachunekBankowy rachunekBankowy;
    Kredyt kredyt;

    @BeforeEach
    void setUp() {
        rachunekBankowy = new RachunekBankowy(1,new OdsetkiA((float) 0.1));
        OperacjaZaciaganiaKredytu operacjaZaciaganiaKredytu = new OperacjaZaciaganiaKredytu(rachunekBankowy,500000,new OdsetkiA((float) 0.1));
        operacjaZaciaganiaKredytu.wykonaj();
        kredyt = operacjaZaciaganiaKredytu.getKredyt();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void splacKredytBrakSrodkow(){
        OperacjaSplatyKredytu operacjaSplatyKredytu = new OperacjaSplatyKredytu(kredyt);
        operacjaSplatyKredytu.wykonaj();
        assertEquals(500000,kredyt.getSaldo());
        assertEquals(0,rachunekBankowy.getSaldo());
    }

    @Test
    void splacKredytDostepneSrodko(){
        rachunekBankowy.wplac(1000000);
        OperacjaSplatyKredytu operacjaSplatyKredytu = new OperacjaSplatyKredytu(kredyt);
        operacjaSplatyKredytu.wykonaj();
        assertEquals(0,kredyt.getSaldo());
        assertEquals(450000,rachunekBankowy.getSaldo());  // 1000000 - (wysokosc kredytu + odsetki)
    }
}