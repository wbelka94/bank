package test.OperacjeBankowe;

import bank.odsetki.OdsetkiB;
import bank.operacje_bankowe.OperacjaTworzeniaDebetu;
import bank.produkty_bankowe.Debet;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OperacjaTworzeniaDebetuTest {

    private RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        rachunekBankowy = new RachunekBankowy(1,new OdsetkiB());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void utworzDebet1000(){
        OperacjaTworzeniaDebetu operacja = new OperacjaTworzeniaDebetu(rachunekBankowy, 1000);
        operacja.wykonaj();
        Debet debet = operacja.getDebet();
        debet.wyplac(100);
        assertEquals(-100,debet.getSaldo());
        debet.wyplac(1000);
        assertEquals(-100,debet.getSaldo());
    }

    @Test
    void utworzDebetMinus10(){
        OperacjaTworzeniaDebetu operacja = new OperacjaTworzeniaDebetu(rachunekBankowy, -10);
        operacja.wykonaj();
        Debet debet = operacja.getDebet();
        assertEquals(0,debet.getUjemneSaldo());
    }
}