package test.OperacjeBankowe;

import bank.odsetki.OdsetkiB;
import bank.operacje_bankowe.OperacjaPrzelewu;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperacjaPrzelewuTest {
    RachunekBankowy rachunekBankowy1;
    RachunekBankowy rachunekBankowy2;

    @BeforeEach
    void setUp() {
        rachunekBankowy1 = new RachunekBankowy(1,new OdsetkiB());
        rachunekBankowy2 = new RachunekBankowy(2,new OdsetkiB());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void przelew100_0(){
        rachunekBankowy1.wplac(100);
        OperacjaPrzelewu operacjaPrzelewu = new OperacjaPrzelewu(rachunekBankowy1,rachunekBankowy2,100);
        rachunekBankowy1.wykonajOperacje(operacjaPrzelewu);
        assertEquals(100,rachunekBankowy2.getSaldo());
        assertEquals(0,rachunekBankowy1.getSaldo());
    }

    @Test
    void przelew0_100(){
        rachunekBankowy2.wplac(100);
        OperacjaPrzelewu operacjaPrzelewu = new OperacjaPrzelewu(rachunekBankowy1,rachunekBankowy2,100);
        rachunekBankowy1.wykonajOperacje(operacjaPrzelewu);
        assertEquals(100,rachunekBankowy2.getSaldo());
        assertEquals(0,rachunekBankowy1.getSaldo());
    }
}