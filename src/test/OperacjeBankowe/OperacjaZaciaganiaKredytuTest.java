package test.OperacjeBankowe;

import bank.odsetki.OdsetkiB;
import bank.operacje_bankowe.OperacjaZaciaganiaKredytu;
import bank.produkty_bankowe.Kredyt;
import bank.produkty_bankowe.RachunekBankowy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperacjaZaciaganiaKredytuTest {
    RachunekBankowy rachunekBankowy;

    @BeforeEach
    void setUp() {
        rachunekBankowy = new RachunekBankowy(1,new OdsetkiB());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void kredyt1000(){
        OperacjaZaciaganiaKredytu operacjaZaciaganiaKredytu = new OperacjaZaciaganiaKredytu(rachunekBankowy,500000,new OdsetkiB());
        operacjaZaciaganiaKredytu.wykonaj();
        Kredyt kredyt = operacjaZaciaganiaKredytu.getKredyt();
        assertEquals(500000,kredyt.getSaldo());
    }
}